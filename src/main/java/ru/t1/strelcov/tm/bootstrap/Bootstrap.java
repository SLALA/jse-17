package ru.t1.strelcov.tm.bootstrap;

import ru.t1.strelcov.tm.api.repository.ICommandRepository;
import ru.t1.strelcov.tm.api.repository.IProjectRepository;
import ru.t1.strelcov.tm.api.repository.ITaskRepository;
import ru.t1.strelcov.tm.api.service.*;
import ru.t1.strelcov.tm.command.AbstractCommand;
import ru.t1.strelcov.tm.command.system.*;
import ru.t1.strelcov.tm.command.task.*;
import ru.t1.strelcov.tm.command.project.*;
import ru.t1.strelcov.tm.repository.CommandRepository;
import ru.t1.strelcov.tm.repository.ProjectRepository;
import ru.t1.strelcov.tm.repository.TaskRepository;
import ru.t1.strelcov.tm.service.*;
import ru.t1.strelcov.tm.util.TerminalUtil;

public class Bootstrap implements ServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ILoggerService loggerService = new LoggerService();

    {
        registry(new DisplayHelpCommand());
        registry(new DisplayAboutCommand());
        registry(new DisplayVersionCommand());
        registry(new DisplaySystemInfoCommand());
        registry(new DisplayArguments());
        registry(new DisplayCommands());
        registry(new ExitCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCompleteByNameCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new ProjectFindByIndexCommand());
        registry(new ProjectFindByIdCommand());
        registry(new ProjectFindByNameCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByNameCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new ProjectUpdateByNameCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCompleteByNameCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskFindByIndexCommand());
        registry(new TaskFindByIdCommand());
        registry(new TaskFindByNameCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByNameCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskUpdateByNameCommand());
        registry(new TaskFindAllByProjectCommand());
        registry(new TaskBindToProjectCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new ProjectRemoveWithTasksCommand());
    }

    public void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initData() {
        final String projectId = projectService.add("p1", "p1").getId();
        projectService.add("p2", "p2");
        taskService.add("t1", "t1").setProjectId(projectId);
        taskService.add("t2", "t2").setProjectId(projectId);
    }

    public void run(String... args) {
        initData();
        displayWelcome();

        try {
            if (parseArgs(args))
                System.exit(0);
        } catch (Exception e) {
            loggerService.errors(e);
            System.exit(0);
        }
        while (true) {
            System.out.println("ENTER COMMAND:");
            try {
                parseCommand(TerminalUtil.nextLine());
                System.out.println("[OK]");
            } catch (Exception e) {
                loggerService.errors(e);
                System.err.println("[FAIL]");
            } finally {
                System.out.println();
            }
        }
    }

    public void displayWelcome() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
    }

    public boolean parseArgs(final String[] args) {
        if (args == null || args.length < 1) return false;
        parseArg(args[0]);
        return true;
    }

    public void parseArg(final String arg) {
        if (arg == null) return;
        loggerService.commands(arg);
        final AbstractCommand abstractCommand = commandService.getCommandByArg(arg);
        abstractCommand.execute();
    }

    public void parseCommand(final String command) {
        if (command == null) return;
        loggerService.commands(command);
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        abstractCommand.execute();
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

}
