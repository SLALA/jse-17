package ru.t1.strelcov.tm.constant;

public class TerminalConst {

    public static final String CMD_HELP = "help";

    public static final String CMD_VERSION = "version";

    public static final String CMD_ABOUT = "about";

    public static final String CMD_INFO = "info";

    public static final String CMD_EXIT = "exit";

    public static final String CMD_COMMANDS = "commands";

    public static final String CMD_ARGUMENTS = "arguments";

    public static final String CMD_TASK_CREATE = "task-create";

    public static final String CMD_TASK_CLEAR = "task-clear";

    public static final String CMD_TASK_LIST = "task-list";

    public static final String CMD_PROJECT_CREATE = "project-create";

    public static final String CMD_PROJECT_CLEAR = "project-clear";

    public static final String CMD_PROJECT_LIST = "project-list";

    public static final String CMD_PROJECT_FIND_BY_ID = "project-find-by-id";

    public static final String CMD_PROJECT_FIND_BY_INDEX = "project-find-by-index";

    public static final String CMD_PROJECT_FIND_BY_NAME = "project-find-by-name";

    public static final String CMD_PROJECT_REMOVE_BY_ID = "project-remove-by-id";

    public static final String CMD_PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";

    public static final String CMD_PROJECT_REMOVE_BY_NAME = "project-remove-by-name";

    public static final String CMD_PROJECT_UPDATE_BY_ID = "project-update-by-id";

    public static final String CMD_PROJECT_UPDATE_BY_INDEX = "project-update-by-index";

    public static final String CMD_PROJECT_UPDATE_BY_NAME = "project-update-by-name";

    public static final String CMD_TASK_FIND_BY_ID = "task-find-by-id";

    public static final String CMD_TASK_FIND_BY_INDEX = "task-find-by-index";

    public static final String CMD_TASK_FIND_BY_NAME = "task-find-by-name";

    public static final String CMD_TASK_REMOVE_BY_ID = "task-remove-by-id";

    public static final String CMD_TASK_REMOVE_BY_INDEX = "task-remove-by-index";

    public static final String CMD_TASK_REMOVE_BY_NAME = "task-remove-by-name";

    public static final String CMD_TASK_UPDATE_BY_ID = "task-update-by-id";

    public static final String CMD_TASK_UPDATE_BY_INDEX = "task-update-by-index";

    public static final String CMD_TASK_UPDATE_BY_NAME = "task-update-by-name";

    public static final String CMD_TASK_START_BY_ID = "task-start-by-id";

    public static final String CMD_TASK_START_BY_INDEX = "task-start-by-index";

    public static final String CMD_TASK_START_BY_NAME = "task-start-by-name";

    public static final String CMD_TASK_COMPLETE_BY_ID = "task-complete-by-id";

    public static final String CMD_TASK_COMPLETE_BY_INDEX = "task-complete-by-index";

    public static final String CMD_TASK_COMPLETE_BY_NAME = "task-complete-by-name";

    public static final String CMD_PROJECT_START_BY_ID = "project-start-by-id";

    public static final String CMD_PROJECT_START_BY_INDEX = "project-start-by-index";

    public static final String CMD_PROJECT_START_BY_NAME = "project-start-by-name";

    public static final String CMD_PROJECT_COMPLETE_BY_ID = "project-complete-by-id";

    public static final String CMD_PROJECT_COMPLETE_BY_INDEX = "project-complete-by-index";

    public static final String CMD_PROJECT_COMPLETE_BY_NAME = "project-complete-by-name";

    public static final String CMD_TASK_BIND_TO_PROJECT = "task-bind-to-project";

    public static final String CMD_TASK_UNBIND_FROM_PROJECT = "task-unbind-from-project";

    public static final String CMD_TASK_FIND_ALL_BY_PROJECT = "task-find-all-by-project";

    public static final String CMD_PROJECT_REMOVE_WITH_TASKS = "project-remove-with-tasks";

}
