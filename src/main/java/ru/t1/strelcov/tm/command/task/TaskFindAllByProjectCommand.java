package ru.t1.strelcov.tm.command.task;

import ru.t1.strelcov.tm.api.service.IProjectTaskService;
import ru.t1.strelcov.tm.model.Task;
import ru.t1.strelcov.tm.util.TerminalUtil;

import java.util.List;

public class TaskFindAllByProjectCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-find-all-by-project";
    }

    @Override
    public String description() {
        return "Find tasks by project id.";
    }

    @Override
    public void execute() {
        final IProjectTaskService projectTaskService = serviceLocator.getProjectTaskService();
        System.out.println("[FIND ALL PROJECT'S TASKS]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = projectTaskService.findAllTasksByProjectId(projectId);
        if (tasks != null) {
            int index = 1;
            for (final Task task : tasks) {
                System.out.println(index + ". " + task);
                index++;
            }
        }
    }

}
