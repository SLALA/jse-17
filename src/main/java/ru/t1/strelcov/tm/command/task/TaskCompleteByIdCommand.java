package ru.t1.strelcov.tm.command.task;

import ru.t1.strelcov.tm.api.service.ITaskService;
import ru.t1.strelcov.tm.exception.entity.TaskNotFoundException;
import ru.t1.strelcov.tm.model.Task;
import ru.t1.strelcov.tm.util.TerminalUtil;

import static ru.t1.strelcov.tm.enumerated.Status.COMPLETED;

public class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-complete-by-id";
    }

    @Override
    public String description() {
        return "Complete task by id.";
    }

    @Override
    public void execute() {
        final ITaskService taskService = serviceLocator.getTaskService();
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER TASK ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.changeStatusById(id, COMPLETED);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

}
