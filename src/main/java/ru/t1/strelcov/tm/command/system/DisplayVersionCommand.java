package ru.t1.strelcov.tm.command.system;

import ru.t1.strelcov.tm.command.AbstractCommand;

public class DisplayVersionCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-v";
    }

    @Override
    public String name() {
        return "version";
    }

    @Override
    public String description() {
        return "Display program version.";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.5.0");
    }

}
