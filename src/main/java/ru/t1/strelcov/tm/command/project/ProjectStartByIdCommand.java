package ru.t1.strelcov.tm.command.project;

import ru.t1.strelcov.tm.api.service.IProjectService;
import ru.t1.strelcov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.strelcov.tm.model.Project;
import ru.t1.strelcov.tm.util.TerminalUtil;

import static ru.t1.strelcov.tm.enumerated.Status.IN_PROGRESS;

public class ProjectStartByIdCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-start-by-id";
    }

    @Override
    public String description() {
        return "Start project by id.";
    }

    @Override
    public void execute() {
        final IProjectService projectService = serviceLocator.getProjectService();
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER TASK ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.changeStatusById(id, IN_PROGRESS);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

}
