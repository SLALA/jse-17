package ru.t1.strelcov.tm.command.system;

import ru.t1.strelcov.tm.api.service.ICommandService;
import ru.t1.strelcov.tm.command.AbstractCommand;

import java.util.Collection;

public class DisplayArguments extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "arguments";
    }

    @Override
    public String description() {
        return "Display arguments.";
    }

    @Override
    public void execute() {
        final ICommandService commandService = serviceLocator.getCommandService();
        System.out.println("[ARGUMENTS]");
        final Collection<AbstractCommand> commands = commandService.getArguments();
        for (final AbstractCommand command : commands) {
            final String name = command.arg();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}
