package ru.t1.strelcov.tm.command.task;

import ru.t1.strelcov.tm.api.service.ITaskService;
import ru.t1.strelcov.tm.exception.entity.TaskNotFoundException;
import ru.t1.strelcov.tm.model.Task;
import ru.t1.strelcov.tm.util.TerminalUtil;

public class TaskUpdateByNameCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-update-by-name";
    }

    @Override
    public String description() {
        return "Update task by name.";
    }

    @Override
    public void execute() {
        final ITaskService taskService = serviceLocator.getTaskService();
        System.out.println("[UPDATE TASK BY NAME]");
        System.out.println("ENTER TASK NAME:");
        final String oldName = TerminalUtil.nextLine();
        System.out.println("ENTER NEW NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER NEW DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.updateByName(oldName, name, description);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

}
