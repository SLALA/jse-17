package ru.t1.strelcov.tm.service;

import ru.t1.strelcov.tm.api.repository.ICommandRepository;
import ru.t1.strelcov.tm.api.service.ICommandService;
import ru.t1.strelcov.tm.command.AbstractCommand;
import ru.t1.strelcov.tm.exception.system.CorruptCommandException;
import ru.t1.strelcov.tm.exception.system.UnknownCommandException;

import java.util.Collection;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    public void add(final AbstractCommand command) {
        final String name = command.name();
        if (name == null || name.isEmpty()) throw new CorruptCommandException();
        commandRepository.add(command);
    }

    @Override
    public AbstractCommand getCommandByName(final String name) {
        final AbstractCommand command = commandRepository.getCommandByName(name);
        if (command == null) throw new UnknownCommandException(name);
        return command;
    }

    @Override
    public AbstractCommand getCommandByArg(String arg) {
        final AbstractCommand command = commandRepository.getCommandByArg(arg);
        if (command == null) throw new UnknownCommandException(arg);
        return command;
    }

    @Override
    public Collection<AbstractCommand> getArguments() {
        return commandRepository.getArguments();
    }

}
