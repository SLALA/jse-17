package ru.t1.strelcov.tm.util;

import ru.t1.strelcov.tm.exception.system.IncorrectIndexException;

import java.util.Scanner;

public class TerminalUtil {

    private static final Scanner SCANNER = new Scanner(System.in);

    public static String nextLine() {
        return SCANNER.nextLine();
    }

    public static Integer nextNumber() {
        final String input = SCANNER.nextLine();
        try {
            return Integer.parseInt(input);
        } catch (Exception e) {
            throw new IncorrectIndexException(input);
        }
    }

}
