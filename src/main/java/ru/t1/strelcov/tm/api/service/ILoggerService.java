package ru.t1.strelcov.tm.api.service;

public interface ILoggerService {

    void info(String message);

    void commands(String message);

    void errors(Exception e);

}
